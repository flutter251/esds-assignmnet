part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class DoLogin extends LoginEvent{
  final String mobileNumber;

  DoLogin({required this.mobileNumber});
}

class SendOtp extends LoginEvent{
  final String mobileNumber;

  SendOtp({required this.mobileNumber});
}

class VerifyUser extends LoginEvent{
  final int enteredOtp;

  VerifyUser({required this.enteredOtp});
}
