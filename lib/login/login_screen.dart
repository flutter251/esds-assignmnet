import 'package:esds_assignment/dashboard/dashboard_screen.dart';
import 'package:esds_assignment/di/injection_container.dart';
import 'package:esds_assignment/registration/registration_screen.dart';
import 'package:esds_assignment/util/text_fields.dart';
import 'package:esds_assignment/util/text_input_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _phoneController;
  late TextEditingController _otpController;
  final LoginBloc _bloc = injector<LoginBloc>();

  bool isOtpSent = false;
  String btnText = "Submit";

  @override
  void initState() {
    super.initState();
    _phoneController = TextEditingController();
    _otpController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, LoginState state) {
              if (state is LoginInitial) {
                const CircularProgressIndicator();
              }

              if (state is LoginResponse) {
                if (state.isUserRegister) {
                  setState(() {
                    _bloc.add(SendOtp(mobileNumber: _phoneController.text));
                  });
                } else {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const RegistrationScreen()));
                }
              }

              if (state is OnOtpSent) {
                if (state.isOtpSent) {
                  isOtpSent = true;
                  btnText = "Verify";
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Something Went Wrong.."),
                  ));
                }
              }

              if (state is OnUserVerification) {
                if (state.isUserVerified) {
                  print("*****************");
                  Navigator.pushReplacementNamed(context, DashboardScreen.route,
                      arguments: {"mobileNumber": _phoneController.text});
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Enter valid OTP"),
                  ));
                }
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 35, right: 35),
                  child: TextFields(
                      text: "Welcome",
                      size: 33.0,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                      alignment: Alignment.centerLeft),
                ),
                const SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: TextInputFields(
                      label: "Phone Number", fieldController: _phoneController),
                ),
                const SizedBox(height: 23),
                Visibility(
                  visible: isOtpSent,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 35, right: 35),
                    child: TextInputFields(
                        label: "Enter OTP", fieldController: _otpController),
                  ),
                ),
                const SizedBox(height: 40),
                ElevatedButton(
                  onPressed: () async {
                    if (btnText == "Submit") {
                      String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                      RegExp regExp = RegExp(pattern);
                      if (_phoneController.text.isNotEmpty &&
                          _phoneController.text.length == 10 &&
                          regExp.hasMatch(_phoneController.text)) {
                        _bloc.add(DoLogin(mobileNumber: _phoneController.text));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Enter valid Mobile Number"),
                          ),
                        );
                        print("Enter valid Mobile Number");
                      }
                    } else {
                      try {
                        _bloc.add(VerifyUser(
                            enteredOtp: int.parse(_otpController.text)));
                      } catch (e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Enter valid OTP "),
                          ),
                        );
                      }
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                      vertical: 12,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Text(
                    btnText,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
