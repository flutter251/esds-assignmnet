part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginResponse extends LoginState {
  final bool isUserRegister;
  LoginResponse({required this.isUserRegister});
}

class OnOtpSent extends LoginState {
  final bool isOtpSent;
  OnOtpSent({required this.isOtpSent});
}

class OnUserVerification extends LoginState {
  final bool isUserVerified;
  OnUserVerification({required this.isUserVerified});
}