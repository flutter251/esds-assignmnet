import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:esds_assignment/repository/repository.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  Repository repository;

  LoginBloc({required this.repository}) : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is DoLogin) {
      bool isUserRegister = await Repository().checkUserRegistered(mobileNumber: event.mobileNumber);
      yield LoginResponse(isUserRegister: isUserRegister);
    }

    if (event is SendOtp) {
      bool isOtpSent = await Repository().sentOtp(mobileNumber: event.mobileNumber);
      yield OnOtpSent(isOtpSent: isOtpSent);
    }

    if (event is VerifyUser) {
      bool isUserVerified = await Repository().verifyUser(enteredOtp: event.enteredOtp);
      yield OnUserVerification(isUserVerified: isUserVerified);
    }
  }

}
