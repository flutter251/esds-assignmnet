import 'package:esds_assignment/dashboard/dashboard_screen.dart';
import 'package:esds_assignment/di/injection_container.dart';
import 'package:esds_assignment/registration/registration_bloc.dart';
import 'package:esds_assignment/util/text_fields.dart';
import 'package:esds_assignment/util/text_input_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _phoneController;
  late TextEditingController _otpController;
  final RegistrationBloc _bloc = injector<RegistrationBloc>();
  bool isOtpSent = false;
  String btnText = "Submit";

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();
    _otpController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, RegistrationState state) {
              if (state is RegistrationInitial) {
                const CircularProgressIndicator();
              }

              if (state is RegistrationResponse) {
                if (state.isUserRegister) {
                  setState(() {
                    _bloc.add(SendOtp(mobileNumber: _phoneController.text));
                  });
                }
              }

              if (state is OnOtpSent) {
                if (state.isOtpSent) {
                  isOtpSent = true;
                  btnText = "Verify";
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Something Went Wrong"),
                    ),
                  );
                }
              }

              if (state is OnUserVerification) {
                if (state.isUserVerified) {
                  Map<String, String> userDetails = {};
                  userDetails["name"] = _nameController.text;
                  userDetails["email"] = _emailController.text;
                  userDetails["mobileNumber"] = _phoneController.text;
                  _bloc.add(SaveUserDetails(
                      mobileNumber: _phoneController.text,
                      userDetails: userDetails));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Enter valid OTP "),
                    ),
                  );
                }
              }

              if (state is ShowDashboard) {
                if (state.isUserSaved) {
                  print("mobile number => ${_phoneController.text}");

                  Navigator.pushReplacementNamed(context, DashboardScreen.route,
                      arguments: {"mobileNumber": _phoneController.text});
                }
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 35, right: 35),
                  child: TextFields(
                      text: "Registration",
                      size: 33.0,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                      alignment: Alignment.centerLeft),
                ),
                const SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: TextInputFields(
                      label: "Name", fieldController: _nameController),
                ),
                const SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: TextInputFields(
                      label: "Email", fieldController: _emailController),
                ),
                const SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: TextInputFields(
                      label: "Phone Number", fieldController: _phoneController),
                ),
                const SizedBox(height: 23),
                Visibility(
                  visible: isOtpSent,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 35, right: 35),
                    child: TextInputFields(
                        label: "Enter OTP", fieldController: _otpController),
                  ),
                ),
                const SizedBox(height: 40),
                ElevatedButton(
                  onPressed: () async {
                    if (btnText == "Submit") {
                      final bool emailValid = RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(_emailController.text);

                      if (emailValid) {
                        String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                        RegExp regExp = RegExp(pattern);
                        if (_phoneController.text.isNotEmpty &&
                            _phoneController.text.length == 10 &&
                            regExp.hasMatch(_phoneController.text)) {
                          _bloc.add(DoRegistration(
                              mobileNumber: _phoneController.text));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text("Enter valid Mobile Number")),
                          );
                          print("Enter valid Mobile Number");
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Enter valid Email"),
                          ),
                        );
                      }
                    } else {
                      try {
                        _bloc.add(VerifyUser(
                            enteredOtp: int.parse(_otpController.text)));
                      } catch (e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Enter valid OTP"),
                          ),
                        );
                      }
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                      vertical: 12,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Text(
                    btnText,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
