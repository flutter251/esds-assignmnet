part of 'registration_bloc.dart';

@immutable
abstract class RegistrationState {}

class RegistrationInitial extends RegistrationState {}

class RegistrationResponse extends RegistrationState {
  final bool isUserRegister;

  RegistrationResponse({required this.isUserRegister});
}

class ShowDashboard extends RegistrationState {
  final bool isUserSaved;

  ShowDashboard({required this.isUserSaved});
}

class OnOtpSent extends RegistrationState {
  final bool isOtpSent;
  OnOtpSent({required this.isOtpSent});
}

class OnUserVerification extends RegistrationState {
  final bool isUserVerified;
  OnUserVerification({required this.isUserVerified});
}