part of 'registration_bloc.dart';

@immutable
abstract class RegistrationEvent {}

class DoRegistration extends RegistrationEvent {
  final String mobileNumber;

  DoRegistration({required this.mobileNumber});
}

class SaveUserDetails extends RegistrationEvent {
  final String mobileNumber;
  final Map<String, String> userDetails;

  SaveUserDetails({required this.mobileNumber, required this.userDetails});
}

class SendOtp extends RegistrationEvent{
  final String mobileNumber;

  SendOtp({required this.mobileNumber});
}

class VerifyUser extends RegistrationEvent{
  final int enteredOtp;

  VerifyUser({required this.enteredOtp});
}