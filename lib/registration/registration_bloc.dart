import 'dart:async';

import 'package:esds_assignment/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'registration_event.dart';

part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  Repository repository;

  RegistrationBloc({required this.repository}) : super(RegistrationInitial());

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    if (event is DoRegistration) {
      bool isUserRegister =
          await Repository().registerNewUser(mobileNumber: event.mobileNumber);
      yield RegistrationResponse(isUserRegister: isUserRegister);
    }

    if (event is SendOtp) {
      bool isOtpSent = await Repository().sentOtp(mobileNumber: event.mobileNumber);
      yield OnOtpSent(isOtpSent: isOtpSent);
    }

    if (event is VerifyUser) {
      bool isUserVerified = await Repository().verifyUser(enteredOtp: event.enteredOtp);
      yield OnUserVerification(isUserVerified: isUserVerified);
    }

    if (event is SaveUserDetails) {
      bool isUserSaved = await Repository().saveUserDetails(
          mobileNumber: event.mobileNumber, details: event.userDetails);
      yield ShowDashboard(isUserSaved: isUserSaved);
    }
  }
}
