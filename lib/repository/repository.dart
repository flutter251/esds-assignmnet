import 'dart:convert';
import 'dart:math';

import 'package:flutter_otp/flutter_otp.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {
  FlutterOtp flutterOtp = FlutterOtp();

  static Future<SharedPreferences> getSharedPreferencesInstance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  Future<bool> getUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool('user_logged_in') ?? false;
    return isLoggedIn;
  }

  Future<bool> checkUserRegistered({required String mobileNumber}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? users = prefs.getStringList('registered_users_list');
    bool isRegistered = users?.contains(mobileNumber) ?? false;
    return isRegistered;
  }

  Future<bool> sentOtp({required String mobileNumber}) async {
    int random = Random().nextInt(1000);
    flutterOtp.sendOtp(mobileNumber,"Here your OTP $random", 1000, 9999, "+91");
    return true;
  }

  Future<bool> verifyUser({required int enteredOtp}) async {
    bool isCorrectOTP = flutterOtp.resultChecker(enteredOtp);
    if (isCorrectOTP) {
      print('Success');
      return true;
    } else {
      print('Failure');
      return true;
    }
  }

  Future<bool> registerNewUser({required String mobileNumber}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> users = prefs.getStringList('registered_users_list') ?? [];

    users.add(mobileNumber);
    prefs.setStringList('registered_users_list', users);
    return true;
  }

  Future<bool> saveUserDetails(
      {required String mobileNumber,
      required Map<String, String> details}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("*****saveUserDetails******");
    print(details);
    String encodedDetails = json.encode(details);
    print(encodedDetails);
    prefs.setString(mobileNumber, encodedDetails);
    return true;
  }

  Future<Map<String, dynamic>> fetchUserDetails(
      {required String mobileNumber}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userDetails = prefs.getString(mobileNumber) ?? "";
    print("*****fetchUserDetails******");
    print(mobileNumber);
    try {
      Map<String, dynamic> details = json.decode(userDetails);
      print(details);
      return details;
    } catch (e) {
      print(e.toString());
      return {};
    }
  }

  Future<bool> setUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_logged_in', true);
    return true;
  }
}
