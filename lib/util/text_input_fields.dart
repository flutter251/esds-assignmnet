import 'package:flutter/material.dart';

class TextInputFields extends StatefulWidget {
  final label;
  final fieldController;

  const TextInputFields({Key? key, this.label, this.fieldController})
      : super(key: key);

  @override
  _TextInputFieldsState createState() => _TextInputFieldsState();
}

class _TextInputFieldsState extends State<TextInputFields> {
  var color = const Color.fromRGBO(232, 232, 232, 1.0);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
        child: Container(
          height: 50,
          child: TextField(
            style: const TextStyle(
              color: Color.fromRGBO(112, 112, 112, 1.0),
              fontSize: 15,
            ),
            controller: widget.fieldController,
            obscureText: false,
            decoration: buildInputDecoration(widget.label),
            autofocus: false,
          ),
          decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(195, 195, 195, 0.16),
                offset: Offset(20.0, 10.0),
                blurRadius: 20.0,
                spreadRadius: 40.0,
              )
            ],
            border: Border(
              left: BorderSide(color: color, width: 4),
              right: const BorderSide(
                  width: 1, color: Color.fromRGBO(239, 239, 239, 1)),
              top: const BorderSide(
                  width: 1, color: Color.fromRGBO(239, 239, 239, 1)),
              bottom: const BorderSide(
                  width: 1, color: Color.fromRGBO(239, 239, 239, 1)),
            ),
          ),
        ));
  }

  InputDecoration buildInputDecoration(label) {
    return InputDecoration(
      contentPadding: const EdgeInsets.only(top: 7, left: 7, bottom: 7),
      labelText: label,
      labelStyle: const TextStyle(
          color: Color.fromRGBO(112, 112, 112, 1.0), fontSize: 15),
      fillColor: const Color.fromRGBO(255, 255, 255, 1),
      border: InputBorder.none,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      errorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      filled: true,
    );
  }
}
