import 'package:flutter/material.dart';

class TextFields extends StatelessWidget {
  final text;
  final size;
  final color;
  final alignment;

  const TextFields({Key? key, this.text, this.size, this.color, this.alignment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      child: Text(
         text,
          overflow: TextOverflow.ellipsis,
          softWrap: false,
          style: TextStyle(fontSize: size,color: color,)),
    );
  }
}
