part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardEvent {}

class FetchUserDetails extends DashboardEvent {
  final String mobileNumber;

  FetchUserDetails({required this.mobileNumber});
}
