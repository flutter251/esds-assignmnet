import 'dart:async';

import 'package:esds_assignment/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'dashboard_event.dart';

part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  Repository repository;

  DashboardBloc({required this.repository}) : super(DashboardInitial());

  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    if (event is FetchUserDetails) {
      Map<String, dynamic> userDetails =
          await repository.fetchUserDetails(mobileNumber: event.mobileNumber);
      yield ShowUserDetails(userDetails: userDetails);
    }
  }
}
