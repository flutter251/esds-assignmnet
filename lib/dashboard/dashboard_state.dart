part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardState {}

class DashboardInitial extends DashboardState {}

class ShowUserDetails extends DashboardState {
  final Map<String, dynamic> userDetails;

  ShowUserDetails({required this.userDetails});
}
