import 'package:esds_assignment/di/injection_container.dart';
import 'package:esds_assignment/login/login_screen.dart';
import 'package:esds_assignment/util/text_input_fields.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dashboard_bloc.dart';

class DashboardScreen extends StatefulWidget {
  static String route = "DashboardScreen";
  final String mobileNumber;

  const DashboardScreen({Key? key, required this.mobileNumber})
      : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _phoneController;

  final DashboardBloc _bloc = injector<DashboardBloc>();

  @override
  void initState() {
    super.initState();

    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();

    print("widget.mobileNumber ${widget.mobileNumber}");
    _bloc.add(FetchUserDetails(mobileNumber: widget.mobileNumber));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, state) {
              if (state is ShowUserDetails) {
                setState(() {
                  _nameController.text = state.userDetails["name"] ?? "";
                  _emailController.text = state.userDetails["email"] ?? "";
                  _phoneController.text =
                      state.userDetails["mobileNumber"] ?? "";
                });
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 150),
                const Text(
                  "Dashboard",
                  style: TextStyle(
                      fontSize: 32,
                      color: Colors.green,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 50),
                Flexible(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 35, right: 35),
                        child: TextInputFields(
                            label: "Name", fieldController: _nameController),
                      ),
                      const SizedBox(height: 23),
                      Padding(
                        padding: const EdgeInsets.only(left: 35, right: 35),
                        child: TextInputFields(
                            label: "Email", fieldController: _emailController),
                      ),
                      const SizedBox(height: 23),
                      Padding(
                        padding: const EdgeInsets.only(left: 35, right: 35),
                        child: TextInputFields(
                            label: "Phone Number",
                            fieldController: _phoneController),
                      ),
                      const SizedBox(height: 40),
                      ElevatedButton(
                        onPressed: () async {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginScreen()));
                        },
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 30,
                            vertical: 12,
                          ),
                          textStyle: const TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        child: const Text(
                          "Logout",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
