
import 'package:esds_assignment/dashboard/dashboard_bloc.dart';
import 'package:esds_assignment/login/login_bloc.dart';
import 'package:esds_assignment/registration/registration_bloc.dart';
import 'package:esds_assignment/repository/repository.dart';
import 'package:get_it/get_it.dart';

final injector = GetIt.instance;

Future<void> init() async {
  injector.registerLazySingleton<Repository>(() => Repository());
  injector.registerFactory<LoginBloc>(() => LoginBloc(repository: injector()));
  injector.registerFactory<DashboardBloc>(() => DashboardBloc(repository: injector()));
  injector.registerFactory<RegistrationBloc>(() => RegistrationBloc(repository: injector()));
}
