// @dart=2.9
import 'package:esds_assignment/registration/registration_screen.dart';
import 'package:flutter/material.dart';
import 'package:esds_assignment/di/injection_container.dart' as di;
import 'package:firebase_core/firebase_core.dart';
import 'dashboard/dashboard_screen.dart';
import 'login/login_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.init();

  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ESDS',
      initialRoute: "/LoginScreen",
      routes: {
        '/LoginScreen': (context) => const LoginScreen(),
        '/RegistrationScreen': (context) => const RegistrationScreen(),
      },
      onGenerateRoute: (settings) {
    if (settings.name == DashboardScreen.route) {
      final args = settings.arguments as Map;
      return MaterialPageRoute(
        builder: (context) {
          return DashboardScreen(
           mobileNumber: args["mobileNumber"],
          );
        },
      );
    }}
  ),
  );
}

